import os
import shutil
import uuid
import tqdm
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.data.automl.utils import get_file_from_path_string, \
    set_extension
from xpresso.ai.core.commons.utils.constants import REPORT_OUTPUT_PATH, \
    EMPTY_STRING
from xpresso.ai.core.data.visualization.report import Report, ReportParam
from xpresso.ai.core.commons.utils.constants import LINE_SPACE


class UnivariateReport(Report):
    def __init__(self, dataset):
        super().__init__(dataset)

    def create_report(self, input_path=utils.DEFAULT_IMAGE_PATH,
                      output_path=REPORT_OUTPUT_PATH,
                      file_name=None,
                      report_format=ReportParam.SINGLEPAGE.value):
        """
        Function to create the report for the metrics of all the attributes
        Args:
            input_path (str): path to the plots to be fetched into the report
            output_path (str): path to the folder the report to be saved
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            file_name(str): file name of report file
        """
        if file_name is None or file_name == EMPTY_STRING:
            file_name = "report_univariate_{}.pdf".format(
                str(uuid.uuid4()))

        output_path, output_file_name = get_file_from_path_string(
            output_path,
            file_name)
        if output_path is None:
            output_path = REPORT_OUTPUT_PATH
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        output_file_name = set_extension(output_file_name, ".pdf")
        self.title_page()
        self.add_univariate_plots(input_path=utils.DEFAULT_IMAGE_PATH,
                                  report_format=report_format,
                                  plot_scatter=False)
        self.output(os.path.join(output_path, output_file_name), 'F')
        print(f'Report saved to {os.path.join(output_path, output_file_name)}')

        if os.path.exists(input_path):
            shutil.rmtree(input_path)

    def add_univariate_plots(self, input_path=utils.DEFAULT_IMAGE_PATH,
                             report_format=ReportParam.SINGLEPAGE.value,
                             plot_scatter=False):
        """
        Helper function to add univariate distribution
        Args:
            input_path (str): path to the plots to be fetched into the report
            report_format (:obj: `ReportParam`): whether the attribute
            distribution should be on same page or continued on different pages
            plot_scatter (bool): Whether scatter plots to be plotted or not
        """
        print("\nPerforming Univariate Report Generation\n")
        if not os.path.exists(input_path):
            print("No univariate plots present for report generation")
            return
        attribute_file_mapping = self.attribute_file_mapping()
        if report_format.lower() == ReportParam.SINGLEPAGE.value:
            for attr in tqdm.tqdm(self.dataset.info.attributeInfo):
                try:
                    univariate_files = attribute_file_mapping[attr.name][
                        "univariate"]
                    target_files = attribute_file_mapping[attr.name]["target"]
                    scatter_files = attribute_file_mapping[attr.name]["scatter"]
                except KeyError:
                    continue
                metrics = self.get_metrics(attr)
                self.add_page()
                self.set_font('Times', 'B', 16)
                # Parameter specification: 0-width(full width), 20-height,
                # text within the cell, 0-No border, 1-cursor on newline,
                # 'C'-center aligned text
                self.cell(0, 20, "Distribution for {}".format(
                    metrics["Attribute name"]), 0, 1, 'C')
                self.ln(LINE_SPACE)  # 10 Line space
                self.table(metrics)
                table_on_page = True
                for file_name in univariate_files:
                    if attr.type != DataType.NUMERIC.value:
                        self.plot_page(file_name=file_name,
                                       input_path=utils.DEFAULT_IMAGE_PATH,
                                       table=table_on_page)
                        table_on_page = False
                        self.add_target_plots(attr.name,
                                              file_names=target_files)
                        continue
                    if "quartile" in file_name or "scatter" in file_name:
                        continue
                    quartile_file_name = ""
                    for file in univariate_files:
                        temp_file = file.split("_")
                        temp_file = "_".join(temp_file[:-3])
                        if "{}_quartile".format(attr.name) == temp_file:
                            quartile_file_name = file
                            break
                    self.plot_numeric(bar_plot_file_name=file_name,
                                      input_path=utils.DEFAULT_IMAGE_PATH,
                                      box_plot_file_name=quartile_file_name,
                                      table=table_on_page)
                    table_on_page = False
                    if plot_scatter:
                        self.add_scatter_plots(file_names=scatter_files)
                    self.add_target_plots(attr.name, file_names=target_files)
        elif report_format.lower() == ReportParam.DOUBLEPAGE.value:
            for attr in tqdm.tqdm(self.dataset.info.attributeInfo):
                try:
                    univariate_files = attribute_file_mapping[attr.name][
                        "univariate"]
                    target_files = attribute_file_mapping[attr.name]["target"]
                    scatter_files = attribute_file_mapping[attr.name]["scatter"]
                except KeyError:
                    continue
                metrics = self.get_metrics(attr)
                self.add_page()
                self.set_font('Times', 'B', 16)
                # Parameter specification: 0-width(full width), 20-height,
                # text within the cell, 0-No border, 1-cursor on newline,
                # 'C'-center aligned text
                self.cell(0, 20, "Distribution for {}".format(
                    metrics["Attribute name"]), 0, 1, 'C')
                self.ln(LINE_SPACE)  # 10 Line space
                self.table(metrics)
                table_on_page = True
                files = univariate_files.append(scatter_files)
                if not files:
                    continue
                for file_name in files:
                    self.plot_page(file_name=file_name,
                                   input_path=utils.DEFAULT_IMAGE_PATH,
                                   table=table_on_page)
                    table_on_page = False
                    self.add_target_plots(attr.name, file_names=target_files)
        else:
            print("Invalid value passed for parameter report_format."
                  "report_format has only three possible values : [single,"
                  "double,None]")

    def plot_page(self, file_name=None, input_path=None, table=False):
        """
        Adds page for provided attribute containing description
            table and distribution plot
        Args:
            file_name (str): file name of the distribution plot for attribute
            input_path (str): path to the folder containing the plot for
                attribute
            table (:obj: `dict`): description data for attribute
        """

        input_path = os.path.abspath(os.path.join(os.getcwd(), input_path))
        if table:
            # Logo (10, 100) coordinates of left top corner of image with
            # width=180mm
            self.image(os.path.join(input_path, file_name), 10, 100, 180)
            return
        self.add_page()
        # Logo (10, 20) coordinates of left top corner of image with
        # width=180mm
        self.image(os.path.join(input_path, file_name), 10, 20, 180)

    def plot_numeric(self, bar_plot_file_name=None, box_plot_file_name=None,
                     input_path=None, table=False):
        """
        Adds page for provided attribute when attribute type is numeric
            containing description table and distribution plot
        Args:
            bar_plot_file_name (str): file name of the distribution plot for
                attribute
            box_plot_file_name (str): file name of the box plot for attribute
            input_path (str): path to the folder containing the plot for
                attribute
            table (:obj: `dict`): description data for attribute
        """
        input_path = os.path.abspath(os.path.join(os.getcwd(), input_path))
        # Logo (50, 110) coordinates of left top corner of image with
        # width=120mm
        self.image(os.path.join(input_path, bar_plot_file_name), 50, 110, 120)
        # Logo (50, 200) coordinates of left top corner of image with
        # width=120mm
        self.image(os.path.join(input_path, box_plot_file_name), 50, 200, 120)
