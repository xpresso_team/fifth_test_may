""" Request Object for API calls """

__all__ = ["XprRequestContext"]
__author__ = "Mrunalini Dhapodkar"

import threading

from xpresso.ai.core.commons.utils.constants import USER_KEY, UID, \
    BLANK_DATA_INDICATOR
from xpresso.ai.core.commons.utils.singleton import Singleton


class XprRequestContext(metaclass=Singleton):
    """
    Defines an Instance operation that lets logger access
    request ID for every API call wrt current thread ID.
    """
    def __init__(self):
        super().__init__()
        self.thread_request_map = {}
        self.context_per_request_id = {}

    def get_context(self):
        request_id = self.get_request_id()
        if not request_id or request_id not in self.context_per_request_id:
            return {}
        return self.context_per_request_id[request_id]

    def set_user_details(self, user_details):
        return self.update_context(context_map={USER_KEY: user_details})

    def get_uid(self):
        request_id = self.get_request_id()
        if not request_id or request_id not in self.context_per_request_id:
            return BLANK_DATA_INDICATOR
        context = self.context_per_request_id[request_id]
        if USER_KEY not in context or UID not in context[USER_KEY]:
            return BLANK_DATA_INDICATOR
        return context[USER_KEY][UID]

    def update_context(self, context_map: dict):
        """
        Update the context for each request id
        Args:
            context_map(dict): key,value pair of global context
        """
        request_id = self.get_request_id()
        if not request_id:
            return {}
        if request_id not in self.context_per_request_id:
            self.context_per_request_id[request_id] = {}
        self.context_per_request_id[request_id].update(context_map)

    def store_request_id(self, request_id):
        """

        Maps request ID to current thread ID and stores this mapping
        Args:
            request_id: Request ID for API call

        """
        thread_id = threading.get_ident()
        self.thread_request_map[thread_id] = request_id

    def get_request_id(self):
        """

        Returns: request ID wrt current thread ID for logging

        """
        if not self.thread_request_map:
            return None
        thread_id = threading.get_ident()
        if thread_id not in self.thread_request_map.keys():
            return None
        return self.thread_request_map[thread_id]
